﻿using System;
using System.Drawing;
using System.IO;
using System.Threading;

namespace ImageProcessor
{
	public class JpgProcessor
	{
		private static readonly Random Random = new Random();

		public void Process(string path)
		{
			Thread.Sleep(Random.Next(200, 1500));
			Bitmap bmp = (Bitmap)Image.FromFile(path);
			for (int i = 0; i < bmp.Size.Width; i++)
			{
				for (int j = 0; j < bmp.Size.Height; j++)
				{
					Color color = bmp.GetPixel(i, j);
					ColorToHSV(color, out double hue, out double saturation, out double value);
					double invertedHue = (hue + 180) % 360;
					bmp.SetPixel(i,j, ColorFromHSV(invertedHue, saturation, value));
				}
			}

			string directoryPath = Path.GetDirectoryName(path);
			string name = Path.GetFileName(path);

			bmp.Save(Path.Combine(directoryPath, $"inverted_{name}"));
		}

		private static void ColorToHSV(Color color, out double hue, out double saturation, out double value)
		{
			int max = Math.Max(color.R, Math.Max(color.G, color.B));
			int min = Math.Min(color.R, Math.Min(color.G, color.B));

			hue = color.GetHue();
			saturation = (max == 0) ? 0 : 1d - (1d * min / max);
			value = max / 255d;
		}

		private static Color ColorFromHSV(double hue, double saturation, double value)
		{
			int hi = Convert.ToInt32(Math.Floor(hue / 60)) % 6;
			double f = hue / 60 - Math.Floor(hue / 60);

			value = value * 255;
			int v = Convert.ToInt32(value);
			int p = Convert.ToInt32(value * (1 - saturation));
			int q = Convert.ToInt32(value * (1 - f * saturation));
			int t = Convert.ToInt32(value * (1 - (1 - f) * saturation));

			if (hi == 0)
				return Color.FromArgb(255, v, t, p);
			else if (hi == 1)
				return Color.FromArgb(255, q, v, p);
			else if (hi == 2)
				return Color.FromArgb(255, p, v, t);
			else if (hi == 3)
				return Color.FromArgb(255, p, q, v);
			else if (hi == 4)
				return Color.FromArgb(255, t, p, v);
			else
				return Color.FromArgb(255, v, p, q);
		}
    }
}
